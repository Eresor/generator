--CREATE DATABASE SystemZlecen
--GO

USE SystemZlecen
GO


DROP TABLE Zlecenie
DROP TABLE TypTowaru
DROP TABLE Klient
DROP TABLE Samochody
DROP TABLE Przyczepy
DROP TABLE Pojazdy
DROP TABLE Oddzial

CREATE TABLE Oddzial
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Nazwa varchar (50),
	Adres varchar (50),
	OddzialNadrzedny INTEGER FOREIGN KEY REFERENCES Oddzial,
)
GO

CREATE TABLE TypTowaru
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Typ varchar (50)
)
GO

CREATE TABLE Klient
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Pesel varchar(50),
	Imie varchar(50),
	Nazwisko varchar(50),
	Adres varchar(50),
	Telefon varchar(50),
	Email varchar(50),
	Wiek varchar(50)
)
GO

CREATE TABLE Pojazdy
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Nazwa varchar (50),
	Rocznik INTEGER,
	LiczbaLatUzytkowania varchar(50),
	AktualnaDostepnosc varchar(50),
	StanTechniczny varchar(50),
	NrVIN varchar(50)
)
GO

CREATE TABLE Przyczepy
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	TypTowaru varchar (50),
	IDPojazdu  INTEGER FOREIGN KEY REFERENCES Pojazdy,
)
GO

CREATE TABLE Samochody
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	TypSamchodu varchar (50),
	MaxIloscPrzyczep INTEGER,
	IDPojazdu  INTEGER FOREIGN KEY REFERENCES Pojazdy
)
GO

CREATE TABLE Zlecenie
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	TypTowaru INTEGER FOREIGN KEY REFERENCES TypTowaru,
	MiejsceNadania varchar(50),
	MiejsceOdbioru varchar(50),
	OczekiwanaDataNadania DATE,
	ZaplanowanaDataOdebrania DATE,
	RzeczywistaDataOdebrania DATE,
	OczekiwanaDataDostarczenia DATE,
	DataZakonczenia DATE,
	LiczbaPrzyczep INTEGER,
	ListaTowarow varchar(500),
	Oddzial INTEGER FOREIGN KEY REFERENCES Oddzial,
	Klient INTEGER FOREIGN KEY REFERENCES Klient,
	Wykonane varchar(50),
	Uwagi varchar(500),
	IDKierowcy INTEGER, 
	IDSamochodu INTEGER FOREIGN KEY REFERENCES Samochody,
	IDPrzyczepy INTEGER FOREIGN KEY REFERENCES Przyczepy,
	NumerTransakcji INTEGER
	
	
)
GO

insert into dbo.TypTowaru values ('Zwyk�e');

insert into dbo.TypTowaru values ('Ludzie');

insert into dbo.TypTowaru values ('Zwierz�ta');

insert into dbo.TypTowaru values ('Niebezpieczne');

insert into dbo.TypTowaru values ('�ywno�c');

insert into dbo.Oddzial values ('Siedziba G��wna', 'Gda�sk', NULL);

insert into dbo.Oddzial values ('Siedziba dodana', 'Hurtowniowo', 1);