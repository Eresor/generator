
--CREATE DATABASE SystemFirmy
--GO

USE SystemFirmy
GO

DROP TABLE PrzyczynaOpoznienia
DROP TABLE ZlecenieTransportu
DROP TABLE ZdobyciePrawaJazdy
DROP TABLE Praca
DROP TABLE Klient
DROP TABLE Samochod
DROP TABLE Przyczepa
DROP TABLE Kierowca
DROP TABLE Opoznienie
DROP TABLE Kategoria
DROP TABLE Miasto
DROP TABLE Data
DROP TABLE Oddzial
DROP TABLE TypTowaru



CREATE TABLE Oddzial
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Nazwa varchar (50),
	Miasto varchar (50),
	OddzialNadrzedny INTEGER FOREIGN KEY REFERENCES Oddzial
)
GO

CREATE TABLE TypTowaru
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Typ varchar (50)
)
GO

CREATE TABLE Klient
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	ImieNazwisko varchar(50),
	Pesel varchar(50),
	Plec varchar(50),
	PrzedzialWiekowy varchar(50),
	Miasto varchar(50),
	Aktualnosc INTEGER
)
GO



CREATE TABLE Samochod
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	MarkaModel varchar (50),
	RokZakupu INTEGER,
	Przedzial_LiczbaLatUzytkowania varchar(50),
	StanTechniczny varchar(50),
	WymaganePrawoJazdy varchar(50),
	MaxLiczbaPrzyczep INTEGER,
	Aktualnosc INTEGER,
	NrVim varchar(50)
)
GO

CREATE TABLE Przyczepa
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	MarkaModel varchar (50),
	RokZakupu INTEGER,
	Przedzial_LiczbaLatUzytkowania varchar(50),
	StanTechniczny varchar(50),
	TypTowaru varchar(50),
	Aktualnosc INTEGER,
	NrVIN varchar(50)
)
GO

insert into Przyczepa values ('Brak przyczepy',0,'','','',1,'')

GO

CREATE TABLE Miasto
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Nazwa varchar(50)
)
GO

CREATE TABLE Opoznienie
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Przyczyna varchar(50)
)
GO


CREATE TABLE Data
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	Data Date,
	Miesiac varchar(50),
	Kwarta� varchar(50),
	PoraRoku varchar(50),
	Rok varchar(50),
	DzienTygodnia varchar(50),
	DzienSwiateczny varchar(50)
)

CREATE TABLE Kierowca
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	ImieNazwisko varchar(50),
	Pesel varchar(50),
	DataZatrudnienia INTEGER FOREIGN KEY REFERENCES Data,
	DataKoncaUmowy INTEGER FOREIGN KEY REFERENCES Data
)
GO

CREATE TABLE Kategoria
(
	ID INTEGER IDENTITY(1,1) PRIMARY KEY,
	NazwaKategorii varchar(50),
	NazwaPodkategorii varchar(50),
	MaterialyNiebezpieczne varchar(50)
)
GO

CREATE TABLE ZdobyciePrawaJazdy
(
	ID_Kierowcy INTEGER FOREIGN KEY REFERENCES Kierowca,
	ID_Kategorii INTEGER FOREIGN KEY REFERENCES Kategoria,
	DataZdobyciaPrawaJazdy INTEGER FOREIGN KEY REFERENCES Data,
)


CREATE TABLE ZlecenieTransportu
(
	ID_Zlecenia INTEGER IDENTITY(1,1) PRIMARY KEY,
	ID_Typu INTEGER FOREIGN KEY REFERENCES TypTowaru,
	ID_MiejsceNadania INTEGER FOREIGN KEY REFERENCES Miasto,
	ID_MiejsceOdbioru INTEGER FOREIGN KEY REFERENCES Miasto,
	ID_OczekiwanaDataNadania INTEGER FOREIGN KEY REFERENCES Data,
	ID_ZaplanowanaDataOdebrania INTEGER FOREIGN KEY REFERENCES Data,
	ID_RzeczywistaDataOdebrania INTEGER FOREIGN KEY REFERENCES Data,
	ID_OczekiwanaDataDostarczenia INTEGER FOREIGN KEY REFERENCES Data,
	ID_DataZakonczenia INTEGER FOREIGN KEY REFERENCES Data,
	ID_Oddzial INTEGER FOREIGN KEY REFERENCES Oddzial,
	ID_Klient INTEGER FOREIGN KEY REFERENCES Klient,
	ID_Kierowcy INTEGER FOREIGN KEY REFERENCES Kierowca,
	ID_Samochodu INTEGER FOREIGN KEY REFERENCES Samochod,
	ID_Przyczepy INTEGER FOREIGN KEY REFERENCES Przyczepa,
	LiczbaPrzyczep INTEGER,
	DlugoscOpoznieniaCalkowitego INTEGER,
	DlugoscOpoznieniaDrogowego INTEGER,
	DlugoscOpoznieniaLogistycznego INTEGER, 
	NumerTransakcji INTEGER
)
GO

CREATE TABLE PrzyczynaOpoznienia
(
	ID_Zlecenia INTEGER FOREIGN KEY REFERENCES ZlecenieTransportu,
	ID_Opoznienia INTEGER FOREIGN KEY REFERENCES Opoznienie
)
GO

CREATE TABLE Praca
(
	ID_Kierowcy INTEGER FOREIGN KEY REFERENCES Kierowca,
	ID_Daty INTEGER FOREIGN KEY REFERENCES Data,
	LiczbaGodzin INTEGER
)
GO
