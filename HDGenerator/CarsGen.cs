﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDGenerator
{
    class CarsGen
    {
        private Random r;
        DateTime time;
        public CarsGen(DateTime t)
        {
            time = t;
            r = new Random(t.GetHashCode());
        }

        public string NextVehicle()
        {
            string insert = "insert into dbo.Pojazdy values ('";
            string name = getName();
            string year = getYear(time);
            string yearb = getYearb(year, time);
            string accessibility = "Tak";
            string Condition = "Sprawny";
            string VIN = getVin();
            string separator = "', '";

            insert += name + separator + year + separator + yearb + separator + accessibility + separator + Condition + separator + VIN +"');\n";

            return insert;
        }
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private string getVin()
        {
            string res = "";
            for(int i = 0; i<13;i++)
            {
                res += r.Next(0, 10).ToString();
            }
            for (int i = 0; i < 4; i++)
            {
                res += chars[r.Next(0, chars.Length - 1)];
            }

            return res;

        }

        public static int GlobalVehiclesCounter = 1;
        public void getSubCars(string[] sideCars, string[] cars, string[] vehicles)
        {
            int numCars = cars.Count();
            int numSidecars = sideCars.Count();

            for (int i = 0; i < numCars; i++)
            {
                cars[i] = "insert into dbo.Samochody values ('";
                string[] carTypes = { "Autobus", "Bus", "Ciężarówka", "Półciężarówka", "TIR", "Dostawczy" };
                int ktory = r.Next(0, carTypes.Count());
                cars[i] += carTypes[ktory] + "', ";
                int ilePrzyczep = r.Next(0, 4);
                if (ilePrzyczep == 0)
                    cars[i] += "0, ";
                else
                    cars[i] += "1, ";
                cars[i] += (GlobalVehiclesCounter).ToString() + ");";
                GlobalVehiclesCounter++;

            }
            int index = 0;
            for (int i = numCars; i < numCars + numSidecars; i++)
            {
                sideCars[index] = "insert into dbo.Przyczepy values ('";
                string[] carTypes = { "Zwykłe", "Ludzie", "Zwierzęta", "Niebezpieczne", "Żywnośc" };
                int ktory = r.Next(0, carTypes.Count());
                sideCars[index] += carTypes[ktory] + "', ";
                sideCars[index] += (GlobalVehiclesCounter).ToString() + ");";
                GlobalVehiclesCounter++;
                index++;
            }
        }

        public string getName()
        {
            string name = "";
            string[] carNames = { "BMW ", "Audi ", "Avia ", "Berliet ", "DAF ", "FIAT ", "Ford ", "Iveco ", "Isuzu ", "KAZ ", "MAZ ", "Volvo " };
            int ktory = r.Next(0, carNames.Count());
            name += carNames[ktory];
            name += r.Next(8, 15).ToString() + ".0 ";

            return name;
        }

        public string getYear(DateTime t)
        {
            string year = r.Next(1970, t.Year).ToString();
            return year;
        }
        public string getYearb(string year, DateTime t)
        {
            int y = Convert.ToInt32(year);

            if (y == t.Year)
                return "1";
            string yearb = (t.Year - r.Next(y, t.Year)).ToString();
            return yearb;
        }

    }
}
