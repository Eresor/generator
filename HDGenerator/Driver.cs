﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDGenerator
{
    class Driver
    {
        public int ID;
        public string Name;
        public string Surname;
        public string Pesel;
        public DateTime StartDate;
        public DateTime EndDate;
        public string Categories;
        public string Phone;
        public bool Able;
        public DateTime ReturnDate;

        public int[] History; //Nullable!

        private static int _indexCounter = -1;

        private static int lastHireOffset = 0;

        private static int IndexCounter
        {
            get
            {
                _indexCounter++;
                return _indexCounter;
            }
        }

        public static void ResetIndex()
        {
            _indexCounter = -1;
        }

        private static readonly string[] categories =
        {
            "C,,", "C,,ADR", "C,+E,", "C,+E,ADR", "D,,", "D,,ADR", "D,+E,", "D,+E,ADR",
            "CD,,", "CD,,ADR", "CD,+E,", "CD,+E,ADR"
        };

        public static Driver NextDriver(DateTime companyStartDate, DateTime minDate, DateTime maxDate)
        {
            Driver dr = new Driver();
            dr.ID = IndexCounter;

            dr.Name = PersonGen.getName();

            dr.Surname = PersonGen.getSurname(dr.Name);

            dr.Pesel = PersonGen.getPesel();

            dr.Phone = PersonGen.getPhone();

            Random r = PersonGen.Rand;

            dr.StartDate = ( r.Next(0,100) < 85 ? minDate : minDate.AddDays(r.Next(lastHireOffset, lastHireOffset + r.Next(30))));
            lastHireOffset = (int)(dr.StartDate - minDate).TotalDays;

            dr.EndDate = dr.StartDate.AddYears(r.Next(3, 10));

            StringBuilder sb = new StringBuilder();
            string cat = categories[r.Next(0, categories.Length)];
            cat = cat.Replace(',', ';');
            sb.Append(cat);
            
            dr.Categories = sb.ToString();

            dr.Able = r.Next(0, 100) < 80;

            if(!dr.Able)
            {
                dr.ReturnDate = DateTime.Today.AddDays(r.Next(7));
            }

            dr.generateHistory(companyStartDate);
            return dr;
        }

        private void generateHistory(DateTime companyStartTime)
        {
            DateTime endTime = (EndDate > DateTime.Today ? DateTime.Today : EndDate);

            int numDays = (int) (endTime - StartDate).TotalDays;

            History = new int[numDays];

            for (int i = 0; i < numDays; i++)
            {

                Random r = PersonGen.Rand;
                int skipChance = r.Next(0, 100);

                if (skipChance < 80)
                {
                    History[i] = r.Next(8, 14);
                }
                else if (skipChance < 90)
                {
                    History[i] = 0;
                    i += 1;
                }
                else if(i < numDays-2)
                {
                    History[i] = 0;
                    History[i+1] = 0;
                    i += 1; //value is also increased in loop
                }
            }

        }

        public string modifyRandomValue()
        {
            Random r = PersonGen.Rand;
            int chance = r.Next(100);
            Categories = categories[r.Next(0, categories.Length)];
            
            if(chance<20)
            {
                Phone = PersonGen.getPhone();
            }
            return this.ID.ToString()+","+Categories+',';
        }

        public string HistoryToString(int breakIdx = 0)
        {
            StringBuilder sb = new StringBuilder();
            char separator = ',';
            sb.Append(ID);
            sb.Append(separator);

            for (int i=0; i<History.Count();i++)
            {
                int? hours = History[i];

                if(breakIdx>0 && breakIdx<=i)
                {
                    break;
                }

                if (hours==null)
                {
                    sb.Append(separator);
                }
                else
                {
                    sb.Append(hours < 10 ? '0' + hours.ToString() : hours.ToString());
                    sb.Append(separator);
                }
            }

            return sb.ToString();
        }

        public override string ToString()
        {
            string separator = ",";
            StringBuilder sb = new StringBuilder();

            sb.Append(ID);
            sb.Append(separator);

            sb.Append(Name);
            sb.Append(separator);

            sb.Append(Surname);
            sb.Append(separator);

            sb.Append(Pesel);
            sb.Append(separator);

            sb.Append(StartDate.ToString("dd-MM-yyyy"));
            sb.Append(separator);

            sb.Append(EndDate.ToString("dd-MM-yyyy"));
            sb.Append(separator);

            sb.Append('\"');

            sb.Append(Categories);

            sb.Append('\"');
            sb.Append(separator);

            sb.Append(Phone);
            sb.Append(separator);

            sb.Append(Able ? "Dostepny" : "Niedostepny" );
            sb.Append(separator);

            if(!Able)
            {
                sb.Append(ReturnDate.ToString("yy-MM-dd"));
            }

            return sb.ToString();
        }
    }
}
