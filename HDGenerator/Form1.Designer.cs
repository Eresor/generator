﻿namespace HDGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ileKlientow = new System.Windows.Forms.TextBox();
            this.ilePojazdow = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ileZamowien = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ileKlientow2 = new System.Windows.Forms.TextBox();
            this.ilePojazdow2 = new System.Windows.Forms.TextBox();
            this.ileZamowien2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.numChangesClients = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerT1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerT0 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numDriversT0 = new System.Windows.Forms.TextBox();
            this.numDriversT1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.numChangesDrivers = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Liczba nowych klientów\r\n";
            // 
            // ileKlientow
            // 
            this.ileKlientow.Location = new System.Drawing.Point(34, 29);
            this.ileKlientow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ileKlientow.Name = "ileKlientow";
            this.ileKlientow.Size = new System.Drawing.Size(89, 20);
            this.ileKlientow.TabIndex = 1;
            this.ileKlientow.Text = "20";
            // 
            // ilePojazdow
            // 
            this.ilePojazdow.Location = new System.Drawing.Point(157, 30);
            this.ilePojazdow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ilePojazdow.Name = "ilePojazdow";
            this.ilePojazdow.Size = new System.Drawing.Size(114, 20);
            this.ilePojazdow.TabIndex = 4;
            this.ilePojazdow.Text = "21";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(154, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Liczba nowych pojazdów";
            // 
            // ileZamowien
            // 
            this.ileZamowien.Location = new System.Drawing.Point(310, 30);
            this.ileZamowien.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ileZamowien.Name = "ileZamowien";
            this.ileZamowien.Size = new System.Drawing.Size(114, 20);
            this.ileZamowien.TabIndex = 7;
            this.ileZamowien.Text = "20";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(308, 13);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Liczba nowych zamówień";
            // 
            // ileKlientow2
            // 
            this.ileKlientow2.Location = new System.Drawing.Point(34, 52);
            this.ileKlientow2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ileKlientow2.Name = "ileKlientow2";
            this.ileKlientow2.Size = new System.Drawing.Size(88, 20);
            this.ileKlientow2.TabIndex = 8;
            this.ileKlientow2.Text = "20";
            // 
            // ilePojazdow2
            // 
            this.ilePojazdow2.Location = new System.Drawing.Point(157, 52);
            this.ilePojazdow2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ilePojazdow2.Name = "ilePojazdow2";
            this.ilePojazdow2.Size = new System.Drawing.Size(114, 20);
            this.ilePojazdow2.TabIndex = 9;
            this.ilePojazdow2.Text = "25";
            // 
            // ileZamowien2
            // 
            this.ileZamowien2.Location = new System.Drawing.Point(310, 52);
            this.ileZamowien2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ileZamowien2.Name = "ileZamowien2";
            this.ileZamowien2.Size = new System.Drawing.Size(114, 20);
            this.ileZamowien2.TabIndex = 10;
            this.ileZamowien2.Text = "20";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "T0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 54);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "T1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(296, 325);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(299, 97);
            this.button1.TabIndex = 13;
            this.button1.Text = "Generuj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // numChangesClients
            // 
            this.numChangesClients.Location = new System.Drawing.Point(34, 113);
            this.numChangesClients.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numChangesClients.Name = "numChangesClients";
            this.numChangesClients.Size = new System.Drawing.Size(89, 20);
            this.numChangesClients.TabIndex = 16;
            this.numChangesClients.Text = "20";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 88);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Liczba zmienionych rekordów";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // dateTimePickerT1
            // 
            this.dateTimePickerT1.CustomFormat = "yyyyMMdd";
            this.dateTimePickerT1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerT1.Location = new System.Drawing.Point(24, 325);
            this.dateTimePickerT1.Name = "dateTimePickerT1";
            this.dateTimePickerT1.Size = new System.Drawing.Size(237, 20);
            this.dateTimePickerT1.TabIndex = 17;
            this.dateTimePickerT1.Value = new System.DateTime(2011, 10, 24, 11, 38, 0, 0);
            // 
            // dateTimePickerT0
            // 
            this.dateTimePickerT0.CustomFormat = "yyyyMMdd";
            this.dateTimePickerT0.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerT0.Location = new System.Drawing.Point(24, 279);
            this.dateTimePickerT0.Name = "dateTimePickerT0";
            this.dateTimePickerT0.Size = new System.Drawing.Size(237, 20);
            this.dateTimePickerT0.TabIndex = 18;
            this.dateTimePickerT0.Value = new System.DateTime(2010, 10, 24, 11, 38, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 254);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Data T0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Data T1";
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.CustomFormat = "yyyyMMdd";
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(24, 222);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(237, 20);
            this.dateTimePickerStart.TabIndex = 21;
            this.dateTimePickerStart.Value = new System.DateTime(2009, 10, 24, 11, 38, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Data rozpoczęcia działaności";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(463, 13);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Liczba nowych kierowców";
            // 
            // numDriversT0
            // 
            this.numDriversT0.Location = new System.Drawing.Point(466, 30);
            this.numDriversT0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numDriversT0.Name = "numDriversT0";
            this.numDriversT0.Size = new System.Drawing.Size(114, 20);
            this.numDriversT0.TabIndex = 24;
            this.numDriversT0.Text = "20";
            // 
            // numDriversT1
            // 
            this.numDriversT1.Location = new System.Drawing.Point(466, 54);
            this.numDriversT1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numDriversT1.Name = "numDriversT1";
            this.numDriversT1.Size = new System.Drawing.Size(114, 20);
            this.numDriversT1.TabIndex = 25;
            this.numDriversT1.Text = "20";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(408, 88);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(209, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Liczba zmienionych rekordów (pracownicy)";
            // 
            // numChangesDrivers
            // 
            this.numChangesDrivers.Location = new System.Drawing.Point(466, 113);
            this.numChangesDrivers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numChangesDrivers.Name = "numChangesDrivers";
            this.numChangesDrivers.Size = new System.Drawing.Size(114, 20);
            this.numChangesDrivers.TabIndex = 27;
            this.numChangesDrivers.Text = "20";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 446);
            this.Controls.Add(this.numChangesDrivers);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.numDriversT1);
            this.Controls.Add(this.numDriversT0);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dateTimePickerStart);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTimePickerT0);
            this.Controls.Add(this.dateTimePickerT1);
            this.Controls.Add(this.numChangesClients);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ileZamowien2);
            this.Controls.Add(this.ilePojazdow2);
            this.Controls.Add(this.ileKlientow2);
            this.Controls.Add(this.ileZamowien);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ilePojazdow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ileKlientow);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ileKlientow;
        private System.Windows.Forms.TextBox ilePojazdow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ileZamowien;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ileKlientow2;
        private System.Windows.Forms.TextBox ilePojazdow2;
        private System.Windows.Forms.TextBox ileZamowien2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox numChangesClients;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerT1;
        private System.Windows.Forms.DateTimePicker dateTimePickerT0;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox numDriversT0;
        private System.Windows.Forms.TextBox numDriversT1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox numChangesDrivers;
    }
}

