﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JayMuntzCom;

namespace HDGenerator
{
    public class Generator
    {
        Random r = new Random();
        //String fileName;
        private String path;
        //private String newName;
        private int nextFile = 1;

        public int NumOrders;

        private string[] clients;
        private string[] vehicles;
        private string[] cars;
        private string[] sideCars;
        private string[] clients2;
        private string[] vehicles2;
        private string[] cars2;
        private string[] sideCars2;
        private string[] orders;

        private Driver[] drivers;

        private string sqlFileT0 = "";
        private string sqlFileT1 = "";

        private string driverDataT0 = "";
        private string driverDataT1 = "";
        private string driverHistoryT0 = "";
        private string driverHistoryT1 = "";
        private string dateFile = "";
        private string newCategoriesFile = "";

        private string extension = "";

        public  DateTime startDate;
        public  DateTime t0;
        public  DateTime t1;

        List<string> holidays = new List<string>();

        public Generator()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.RootFolder = System.Environment.SpecialFolder.MyComputer;
            fbd.ShowDialog();
            Program.rootPath = fbd.SelectedPath + "/";
            path = Program.rootPath;
           


        }

        private string CreateFile(string filename)
        {
            try
            {
                FileStream fs = new FileStream(path + "/" + filename, FileMode.CreateNew);
                fs.Close();
            }
            catch(IOException r)
            {
                string name = Path.GetFullPath(filename);
                extension = Path.GetExtension(name);
                name = Path.GetFileNameWithoutExtension(name);
                filename = name  + "(" + nextFile++.ToString() + ")" + extension;
                CreateFile(filename);
            }
            return filename;
        }

        public void Generate(DateTime t0, DateTime t1)
        {
            sqlFileT0 = CreateFile("T0.sql");
            nextFile = 1;
            sqlFileT1 = CreateFile("T1.sql");

            nextFile = 1;
            driverDataT0 = CreateFile("driversDataT0.csv");
            nextFile = 1;
            driverDataT1 = CreateFile("driversDataT1.csv");
            nextFile = 1;
            driverHistoryT0 = CreateFile("driversHistoryT0.csv");
            nextFile = 1;
            driverHistoryT1 = CreateFile("driversHistoryT1.csv");
            nextFile = 1;
            dateFile = CreateFile("DateFile.txt");
            nextFile = 1;
            newCategoriesFile = CreateFile("NewCategories.txt");

            this.t0 = t0;
            this.t1 = t1;
            this.startDate = t0.AddDays(-365);
            // ClientGen cg = new ClientGen();
            // Console.WriteLine(cg.NextClient());
        }

        private T[] newArray <T> (T[] a, T[] b)
        {
            List<T> list = new List<T>();
            list.AddRange(a);
            list.AddRange(b);
            return list.ToArray();
        }

        DateTime RandomDate(DateTime from, DateTime to)
        {
            DateTime start = from;
            int range = (to - start).Days;
            return start.AddDays(r.Next(range));
        }

        public void clientsGenerate(int ile, int ile2)
        {
            PersonGen cg = new PersonGen(this);
            clients = new string[ile];
            for(int i = 0; i < ile;i++)
            {
                clients[i] = cg.NextClient();
            }

            clients2 = new string[ile2];
            for (int i = 0; i < ile2; i++)
            {
                clients2[i] = cg.NextClient();
            }

            File.WriteAllLines(path + "/" + sqlFileT0, clients);
            File.WriteAllLines(path + "/" + sqlFileT1, clients2);
            //clients = newArray<string>(clients, clients2);
            Console.WriteLine("Clients generated!");
        }

        public void carsGenerate(int ile, int ile2)
        {
            CarsGen cg = new CarsGen(t0);

            vehicles = new string[ile];
            for (int i = 0; i < ile; i++)
            {
                vehicles[i] = cg.NextVehicle();
            }
            File.AppendAllLines(path + "/" + sqlFileT0, vehicles);
            cg = new CarsGen(t1);
            vehicles2 = new string[ile2];
            for (int i = 0; i < ile2; i++)
            {
                vehicles2[i] = cg.NextVehicle();
            }
            File.AppendAllLines(path + "/" + sqlFileT1, vehicles2);
            

            int numCars = r.Next(vehicles.Count() / 2, 3 * vehicles.Count() / 5);
            int numSidecars = vehicles.Count() - numCars;

            cars = new string[numCars];
            sideCars = new string[numSidecars];

            cars2 = new string[numCars];
            sideCars2 = new string[numSidecars];

            cg.getSubCars(sideCars, cars, vehicles);
            File.AppendAllLines(path + "/" + sqlFileT0, cars);
            File.AppendAllLines(path + "/" + sqlFileT0, sideCars);

            cg.getSubCars(sideCars2, cars2, vehicles2);
            File.AppendAllLines(path + "/" + sqlFileT1, cars2);
            File.AppendAllLines(path + "/" + sqlFileT1, sideCars2);

           // cars = newArray<string>(cars, cars2);
           // sideCars = newArray<string>(sideCars, sideCars2);
           // vehicles = newArray<string>(vehicles, vehicles2);

            Console.WriteLine("Cars generated!");
        }

        public void ordersGenerate(int ile, int ile2)
        {
            NumOrders = ile;
            OrderGen og = new OrderGen(clients, cars, sideCars,startDate,t0,this);
            orders = new string[ile];
            for (int i = 0; i < ile; i++)
            {
                orders[i] = og.NextOrder();
            }

            File.AppendAllLines(path + "/" + sqlFileT0, orders);

            NumOrders = ile2;
            string []orders2 = new string[ile2];
            og.t0 = t0;
            og.t1 = t1;
            cars = newArray<string>(cars, cars2);
            clients = newArray<string>(clients, clients2);
            vehicles = newArray<string>(vehicles, vehicles2);
            sideCars = newArray<string>(sideCars, sideCars2);

            for (int i = 0; i < ile2; i++)
            {
                orders2[i] = og.NextOrder();
            }

            File.AppendAllLines(path + "/" + sqlFileT1, orders2);

            orders = newArray<string>(orders, orders2);
            Console.WriteLine("Orders generated!");
        }

        public void driversGenerate(int num, int num1, int numChanges)
        {
            Driver.ResetIndex();
            DateTime companyStartDate = startDate;
            PersonGen pg = new PersonGen(this);
            Driver[] driversT0 = new Driver[num];

            for (int i=0;i<num;i++)
            {
                driversT0[i] = Driver.NextDriver(companyStartDate, companyStartDate, t0);   //?? not sure
            }

            Driver[] driversT1 = new Driver[num1];
            for (int i = 0; i < num1; i++)
            {
                driversT1[i] = Driver.NextDriver(companyStartDate, t0, t1);
            }

            drivers = newArray<Driver>(driversT0, driversT1);

            //t0

            File.WriteAllLines(path + "/" + driverDataT0, new string[] { @"ID,Imie,Nazwisko,Pesel,DataZatrudnienia,DataZakonczenia,Kategoria, NumerTelefonu,Dostepnosc,DataPowrotu" } );

            string[] stringDrivers = new string[num];
            List<string> newCategoriList = new List<string>();
            newCategoriList.Add("ID_Kierowcy,Kategoria,Podkategoria,MateriałyNiebezpieczne,DataZdobycia");
            for(int i=0;i<num;i++)
            {
                stringDrivers[i] = driversT0[i].ToString();
            }
            File.AppendAllLines(path + "/" + driverDataT0, stringDrivers);

            //random mod

            for(int i=0;i<numChanges;i++)
            {
                int index = r.Next(num);
                newCategoriList.Add(drivers[index].modifyRandomValue()+RandomDate(t0,t1).ToString("yyyy-MM-dd"));
                stringDrivers[index] = drivers[index].ToString();
            }

            //t1

            File.WriteAllLines(path + "/" + newCategoriesFile, newCategoriList.ToArray());
            File.WriteAllLines(path + "/" + driverDataT1, new string[] { @"ID,Imie,Nazwisko,Pesel,DataZatrudnienia,DataZakonczenia,Kategoria, NumerTelefonu,Dostepnosc,DataPowrotu" });
            File.AppendAllLines(path + "/" + driverDataT1, stringDrivers);

            stringDrivers = new string[num1];
            for (int i = 0; i < num1; i++)
            {
                stringDrivers[i] = driversT1[i].ToString();
            }
            File.AppendAllLines(path + "/" + driverDataT1, stringDrivers);


            //driver's history
            //first

            int numDays = (int)(t1 - companyStartDate).TotalDays;
            StringBuilder header = new StringBuilder("ID,Data,Liczba godzin");
            string format = "yyyy-MM-dd";


            List<string> driversHistoryString = new List<string>();
            driversHistoryString.Add(header.ToString());
            foreach (var driver in driversT0)
            {
                int i = 0;
                foreach (var numHours in driver.History)
                {
                    driversHistoryString.Add(driver.ID.ToString()+','+(driver.StartDate.AddDays(i)).ToString(format)+','+numHours);
                    i++;
                }
            }

            File.AppendAllLines(path+'/'+driverHistoryT0,driversHistoryString);


            //second
            driversHistoryString.Clear();
            driversHistoryString.Add(header.ToString());
            foreach (var driver in driversT1)
            {
                int i = 0;
                foreach (var numHours in driver.History)
                {
                    driversHistoryString.Add(driver.ID.ToString() + ',' + (driver.StartDate.AddDays(i)).ToString(format) + ',' + numHours);
                    i++;
                }
            }

            File.AppendAllLines(path+'/'+driverHistoryT1,driversHistoryString);

            Console.WriteLine("Drivers generated!");
        }

        private string getSeason(int month)
        {
            if (month <=2  || month == 12)
            {
                return "Zima";
            }
            else if (month <= 5)
            {
                return "Wiosna";
            }
            else if (month <= 8)
            {
                return "Lato";
            }
            else
            {
                return "Jesień";
            }
        }


        public void dateGenerate()
        {
            for (int year = startDate.Year; year <= t1.Year; year++)
            {
                holidays.Add(new DateTime(year, 1, 1).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 1, 6).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 3, 27).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 3, 28).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 5, 1).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 5, 3).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 5, 15).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 5, 26).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 8, 15).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 11, 1).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 11, 11).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 12, 25).ToString("yyyy-MM-dd,"));
                holidays.Add(new DateTime(year, 12, 26).ToString("yyyy-MM-dd,"));


            }


            List<string> list = new List<string>();
            StringBuilder sb = new StringBuilder("Data,Miesiac,Kwartal,PoraRoku,Rok,DzienTygodnia,DzienSwiateczny");
            list.Add(sb.ToString());
            for (DateTime day = startDate; day <= t1.AddYears(10);day =  day.AddDays(1))
            {
                sb.Clear();
                sb.Append(day.Date.ToString("yyyy-MM-dd,"));
                sb.Append(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(day.Month));
                sb.Append(", Kwartał: ");
                sb.Append(day.Month/4 + 1);
                sb.Append(",");
                sb.Append(getSeason(day.Month));
                sb.Append(',');
                sb.Append(day.Year);
                sb.Append(',');
                sb.Append(day.ToString("dddddddd",new CultureInfo("pl-PL")));
                if (holidays.Contains(day.Date.ToString("yyyy-MM-dd,")))
                    sb.Append(",Dzień świąteczny");
                else
                sb.Append(",Dzień zwykły");
                list.Add(sb.ToString());
            }

            File.WriteAllLines(path + "/" + dateFile,list.ToArray());
        }

        public int LastSelectedDriverID;
        public DateTime getFirstAbleDate(DateTime from, int questDays, string category) //do użycia PO wydrukowaniu historii lub zrobieniu kopii
        {
            //int idx = DateToHistoryIdx(from);
            //int max = DateToHistoryIdx(t1)-questDays-1;
            //for(int i=idx;i<max;i++)
            //{
            //    foreach(Driver d in drivers)
            //    {
            //        if(d.Categories.Contains(category) && d.History[i]>0)
            //        {
            //            for(int j=0;j< questDays;j++)
            //            {
            //                d.History[i+j] = 0; //testowo oznaczanie "użytych" kierowców
            //            }
            //            LastSelectedDriverID = d.ID;
            //            return from.AddDays(i - idx);
            //        }
            //    }
            //}
            return from.AddDays(r.Next(3));
        }

        private int DateToHistoryIdx(DateTime date)
        {
            return (int)(date - startDate).TotalDays;
        }

        public void changeRecords(int ile)
        {
            for (int i = 0; i < ile ;  i++)
            {
                int wynik = r.Next(0, 3);
                switch (wynik)
                {
                    case 0:
                        changeClient();
                        break;
                    case 1:
                        changeCar();
                        break;
                    case 2:
                        changeOrder();
                        break;

                }
            }

            Console.WriteLine("Orders modified!");
        }

        private void changeClient()
        {
            int ID = r.Next(0, clients.Length);
            string[] temp = clients[ID].Split('\'');
            // 1 - name
            // 3 - sname
            // 5 - city
            // 7 - phone
            // 9 - email

            PersonGen cg = new PersonGen(this);
            int changeID = r.Next(0, 4);
            string wynik = "UPDATE dbo.Klient SET";

            switch (changeID)
            {
                case 0:
                    temp[3] = PersonGen.getSurname(temp[1]);
                    wynik += " Nazwisko = '" + temp[3];
                    break;
                case 1:
                    temp[5] = PersonGen.getAdres();
                    wynik += " Adres = '" + temp[5];
                    break;
                case 2:
                    temp[7] = PersonGen.getPhone();
                    wynik += " Telefon = '" + temp[7];
                    break;
                case 3:
                    temp[9] = cg.getEmail(temp[1],temp[3]);
                    wynik += " Email = '" + temp[9];
                    break;
            }

            wynik += "' WHERE ID = " + (ID + 1).ToString();

            File.AppendAllLines(path + "/" + sqlFileT1, new string[] { wynik });


        }

        private void changeOrder()
        {

        }

        private void changeCar()
        {
            int ID = r.Next(0, clients.Length);
            int changeID = r.Next(0, 2);


            string wynik = "UPDATE dbo.Pojazdy SET";

            switch (changeID)
            {
                case 0:
                    wynik += " AktualnaDostepnosc = 'Nie'";
                    break;
                case 1:
                    wynik += " StanTechniczny = 'Uszkodzony'";
                    break;


            }

            wynik += " WHERE ID = " + (ID + 1).ToString();

            File.AppendAllLines(path + "/" + sqlFileT1, new string[] { wynik });
        }


    }
}
