﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDGenerator
{
    class OrderGen
    {
        Random r = new Random();
        int ktory;
        private string[] clients;
        private string[] cars;
        private string[] sideCars;
        private bool driverDelay = false;
        private bool carDelay = false;
        private bool malfunctionDelay = false;
        private bool accidentDelay = false;
        private int limit = 0;

        private DateTime _t0;
        public DateTime t0
        {
            set
            {
                currentIterationDate = _t0;
                _t0 = value;
            }
            get
            {
                return _t0;
            }
        }


        public DateTime t1;
        string[] carTypes = { "Zwykłe", "Ludzie", "Zwierzęta", "Niebezpieczne", "Żywnośc" };
        string typ;
        private Generator gen;

        private DateTime currentIterationDate;

        public OrderGen(string[] clients, string[] cars, string[] sideCars,DateTime t0,DateTime t1,Generator gen)
        {
            this.clients = clients;
            this.cars = cars;
            this.sideCars = sideCars;
            this.t0 = t0;
            this.t1 = t1;
            this.gen = gen;
            currentIterationDate = t0;
        }

        public static int GlobalOrderCounter = 1;
        public string NextOrder()
        {
            string insert = "insert into dbo.Zlecenie values (";
            typ = getTyp();
            string source = getSource();
            string dest = getSource();
            carDelay = false;
            malfunctionDelay = false;
            accidentDelay = false;

            string sideCarsNumber = getSideCar();
            string productList = getProductList();
            string unit = "1";
            string client = getClient();
            //////////////////////////////////////////////////////////////////
            
            
            string[] dates = getDates(t0,t1,Convert.ToInt32(typ)-1,Convert.ToInt32(sideCarsNumber));


            string driverID = gen.LastSelectedDriverID.ToString();//getDriverID();



            //////////////////////////////////////////////////////////////////
            string isDone = "Yes";
            string notice = getNotice();
            string carID = getCarID();
            string sideCarID = getSideCarID(Convert.ToInt32(sideCarsNumber));

            string separator = "', '";
            insert += typ + ", '" + source + separator + dest + separator + dates[0] + separator + dates[1]
                + separator + dates[2] + separator + dates[3] + separator + dates[4] + "'," + sideCarsNumber + ",'" + productList
                + "', " + unit + "," + client + ",'" + isDone + separator + notice + "', " + driverID + ", " + carID + ", " + sideCarID 
                + ", " + GlobalOrderCounter + ");";

            GlobalOrderCounter++;
            return insert;

        }
        
        /*public string[] GetScheduleForCurrentWeek()
        {

        }*/

        private string getNotice()
        {
            string notice = "";
            int ile = 0;
            if (driverDelay)
            {
                notice += "brak kierowcy";
                ile++;
            }
            if (carDelay)
            {
                if (ile != 0)
                    notice += ", brak pojazdu";
                else
                {
                    notice += "brak pojazdu";
                    ile++;
                }
            }
            if (malfunctionDelay)
            {
                if (ile != 0)
                    notice += ", awaria";
                else
                {
                    notice += "awaria";
                    ile++;
                }
            }
            if (accidentDelay)
            {
                if (ile != 0)
                    notice += ", wypadek";
                else
                {
                    notice += "wypadek";
                    ile++;
                }
            }
            return notice;

        }

        private string getCarID()
        {

            int max = cars.Count();
            int ID = r.Next(0, max);

            if (limit > 100)
            {
                limit = 0;
                return (ID + 1).ToString();
            }

            if (typ == "1")
            {
                if (cars[ID].Contains("Bus"))
                {
                    return (ID + 1).ToString();
                }
                else
                {
                    limit++;
                    return getCarID();
                }
            }
            else
            {
                if (!cars[ID].ToLower().Contains("bus"))
                {
                    return (ID + 1).ToString();
                }
                else
                {
                    limit++;
                    return getCarID();
                }
            }

        }
        private string getSideCarID(int nr)
        {
            if (nr == 0)
                return "NULL";

            int max = sideCars.Count();
            int ID = r.Next(0, max);

            if (sideCars[ID].Contains(carTypes[Convert.ToInt32(typ) - 1]))
            {
                return (ID + 1).ToString();
            }
            else
            {
                limit++;

                if (limit > 100)
                {
                    limit = 0;
                    return (ID + 1).ToString();
                }
                else
                {
                    return getSideCarID(nr);
                }
            }
        }

        private string getDriverID()
        {
            //TODO
            int max = clients.Count();
            int ID = r.Next(1, max + 1);
            return ID.ToString();

        }

        private string getClient()
        {
            int max = clients.Count();
            int ID = r.Next(1, max + 1);
            //string[] test = clients[ID-1].Split('\'');

            return ID.ToString();
        }

        private string getProductList()
        {
            string products = "";
            switch(ktory)
            {
                case 0:
                    products = "Produkt A x" + r.Next(1, 200).ToString();
                    break;
                case 1:
                    products = "Ludzie x" + r.Next(1, 40).ToString();
                    break;
                case 2:
                    products = "Koń x" + r.Next(1, 4).ToString();
                    break;
                case 3:
                    products = "Trotyl " + r.Next(1, 10).ToString() + " kg";
                    break;
                case 4:
                    products = "Pączek x" + r.Next(200, 800).ToString();
                    break;
            }
            return products;
        }

        private string getSideCar()
        {
            string sideCar = "";
            if (ktory == 1)
                sideCar = "0";
            else
                sideCar = "1";

            return sideCar;
        }

        private string getTyp()
        {
            
            ktory = r.Next(0, carTypes.Count());

            return (ktory+1).ToString();
        }

        private string getSource()
        {
            var lines = File.ReadAllLines(Program.rootPath + "miasta.txt", Encoding.GetEncoding("Windows-1250"));
            int ile = lines.Count();
            return lines[r.Next(0, ile)];
        }

        private string typeToLicenseCategory(int type, int numSideCars)
        {
            switch (type)
            {
                case 0:
                case 2:
                case 4:
                    return (numSideCars > 0 ? "C+E" : "C" );
                case 1:
                    return (numSideCars > 0 ? "D+E" : "D");
                case 3:
                    return "ADR";
                default:
                    return  "D";
            }   
        }

        private string[] getDates(DateTime t0, DateTime t1, int type, int numSideCars)
        {
            string[] dates = new string[5];

            DateTime start = currentIterationDate;
            currentIterationDate = currentIterationDate.AddDays(r.Next(0, 2));//(r.Next(0, 100) < (t0 - t1).TotalDays / gen.NumOrders ? currentIterationDate.AddDays(1) : currentIterationDate);
            DateTime planned, real, expected, done;

            int range = r.Next(1, 3);
            string cat = typeToLicenseCategory(type, numSideCars);

            planned = gen.getFirstAbleDate(currentIterationDate,range,cat);

            int driverDelayTime = 0;

            int isDelay = r.Next(0, 7);
            if(isDelay==0)
            //if((driverDelayTime = (int)(planned - start).TotalDays)>0)
            {
                driverDelay = true;
            }

            int carDelayTime = r.Next(0, 120);
            if (carDelayTime < 10)
            {
                carDelay = true;
                carDelayTime = r.Next(0, 4);
                planned.AddDays(carDelayTime);
            }


            isDelay = r.Next(0, 7);
            if (isDelay == 0)
            {
                real = planned.AddDays(r.Next(1, 3));
                int delay = r.Next(0, 2);
                if (delay == 0)
                    malfunctionDelay = true;
                else
                    accidentDelay = true;
            }
            else
                real = planned;

            expected = real.AddDays(r.Next(0, 14));

            isDelay = r.Next(0, 7);
            if (isDelay == 0)
            {
                done = expected.AddDays(range);
                int delay = r.Next(0, 2);
                if (delay == 0)
                    malfunctionDelay = true;
                else
                    accidentDelay = true;
            }
            else
                done = expected;

            dates[0] = start.ToString("yyyyMMdd");
            dates[1] = planned.ToString("yyyyMMdd");
            dates[2] = real.ToString("yyyyMMdd");
            dates[3] = expected.ToString("yyyyMMdd");
            dates[4] = done.ToString("yyyyMMdd");

            return dates;
        }


    }
}
