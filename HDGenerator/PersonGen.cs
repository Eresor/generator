﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDGenerator
{
    public class PersonGen
    {
        public static Random Rand = new Random();
        private Generator g;
        public PersonGen(Generator g)
        {
            this.g = g;
        }

        public string pesel;
        public string NextClient()
        {
            string insert = "insert into dbo.Klient values ('";
            pesel = getPesel();
            string name = getName();
            string sname = getSurname(name);
            string adres = getAdres();
            string telefon = getPhone();
            string email = getEmail(name,sname);
            string wiek = getWiek();
            string separator = "', '";

            insert += pesel + separator + name + separator +  sname + separator + adres + separator +  telefon + separator + email+ separator+wiek+"');\n";
            return insert;
        }

        public string getWiek()
        {
            return (g.startDate.Year - Convert.ToInt32("19"+ pesel[0] + pesel [1] )).ToString();
        }

        public static string getPesel()
        {
            int year = Rand.Next(70, 95);
            StringBuilder sb = new StringBuilder();
            sb.Append(year);

            int num = Rand.Next(1, 13);
            sb.Append((num < 10 ? '0' + num.ToString() : num.ToString()));

            num = Rand.Next(1, 29);
            sb.Append( (num < 10 ? '0'+num.ToString() : num.ToString() ) );

            for (int i=0;i<5;i++)
            {
                sb.Append(Rand.Next(0, 9));
            }
            return sb.ToString();
        }

        public string getEmail(string name, string sname)
        {
            string email = "";
            int ile = Rand.Next(1, name.Length);

            for(int i =0;i<ile;i++)
            {
                email += name[i];
            }

            int ile2 = Rand.Next(1, sname.Length);
            sname = sname.ToLower();
            for (int i = 0; i < ile2; i++)
            {
                email += sname[i];
            }



            email += "@";

            string[] servers = { "gmail.com", "onet.pl", "wp.pl", "poczta.pl", "ps.com", "skrzynka.net" };
            int ktory = Rand.Next(0, servers.Count());
            email += servers[ktory];
            return email;

        }

        public static string getPhone()
        {
            string nr = "";
            nr += Rand.Next(1, 9);

            for (int i = 0; i < 8; i++)
                nr += Rand.Next(0, 9);

            return nr;
        }

        public static string getAdres()
        {
            var lines = File.ReadAllLines(Program.rootPath + "miasta.txt", Encoding.GetEncoding("Windows-1250"));
            int ile = lines.Count();
            return lines[Rand.Next(0, ile)];
        }

        public static string getName()
        {
            var lines = File.ReadAllLines(Program.rootPath + "imiona.txt", Encoding.GetEncoding("Windows-1250"));
            int ile = lines.Count();
            return new string(lines[Rand.Next(0, ile)].ToCharArray()
                .Where(c=>!Char.IsWhiteSpace(c))
                .ToArray());
        }

        public static string getSurname(string name)
        {
            var lines = File.ReadAllLines(Program.rootPath + "nazwiska.txt", Encoding.GetEncoding("Windows-1250"));
            int ile = lines.Count();
            string temp = lines[Rand.Next(0, ile)];
            if(name.Last().Equals('a'))
            {
                if (temp.Last().Equals('i'))
                    temp = temp.Remove(temp.Length - 1, 1) + "a";
            }
            return temp;
        }

    }
}
